package model.character;

import java.util.*;
import java.util.stream.Collectors;
import model.attribute.Attribute;
import model.attribute.feature.Trait;
import utilities.Utilities;

class CharacterImpl implements Character {

    private final Set<Attribute> attributes;

    CharacterImpl(final Set<Attribute> attributes) {
        Utilities.requireNonNull(attributes);
        if (attributes.isEmpty()) {
            throw new IllegalArgumentException("Characters shall have one Attribute at least");
        }
        this.attributes = attributes;
    }

    @Override
    public String getName() {
        try {
            return attributes.stream().filter(a -> a.getTrait().equals(Trait.NAME)).findFirst().get()
                    .getFeatures().stream().map(f -> f.toString()).collect(Collectors.joining());
        } catch (NoSuchElementException ignored) {
            return "";
        }
    }

    @Override
    public Set<Attribute> getAttributes() {
        return Collections.unmodifiableSet(attributes);
    }

    @Override
    public boolean has(final Attribute attribute) {
        Utilities.requireNonNull(attribute);
        return attributes.stream().anyMatch(attr -> attr.getTrait().equals(attribute.getTrait()) 
                && attr.getFeatures().containsAll(attribute.getFeatures()));
    }

    @Override
    public String toString() {
        return "[" + attributes.stream().map(a -> a.toString()).collect(Collectors.joining("/")) + "]";
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.attributes);
    }

    @Override
    public boolean equals(final Object obj) {
        return obj instanceof Character ? attributes.equals(((Character) obj).getAttributes()) : false;
    }

}
