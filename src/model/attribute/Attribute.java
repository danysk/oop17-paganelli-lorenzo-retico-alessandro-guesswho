package model.attribute;

import java.util.Set;
import model.attribute.feature.Trait;

/** 
 * Modeling interface for an Attribute, composed by a Trait and a Set of different Objects representing its features.
 * Note that an Attribute may not implement all its Trait features, but it can't implement a single feature twice.
 * Attributes can be obtained using AttributeFactory.
 */
public interface Attribute {

    /**
     * Getter method.
     * @return the Trait this attribute describes 
     *          for instance: Hairs, Beard..
     */
    Trait getTrait();

    /**
     * Getter method.
     * @return an unmodifiable Set of Objects representing the Attribute's Set of features
     *          for instance: Color.BLACK, HairStyle.CURLY..
     */
    Set<?> getFeatures();

}
