package model.attribute;

import java.util.*;
import model.attribute.feature.Trait;
import utilities.Utilities;
import java.util.stream.*;

class AttributeImpl implements Attribute {

    private final Trait trait;
    private final Set<?> features;

    AttributeImpl(final Trait trait, final Set<?> features) {
        Utilities.requireNonNull(trait, features);
        if (features.stream().anyMatch(feature -> trait.getDomains().stream().noneMatch(domain -> 
            domain.getType().equals(feature.getClass())))) {
            throw new IllegalArgumentException("Not supported features");
        }
        if (features.stream().anyMatch(feature -> features.stream().anyMatch(f -> 
            !f.equals(feature) && f.getClass().equals(feature.getClass())))) {
            throw new IllegalArgumentException("Multiple features belonging to the same domain");
        }
        this.trait = trait;
        this.features = features;
    }

    @Override
    public Trait getTrait() {
        return this.trait;
    }

    @Override
    public Set<?> getFeatures() {
        return Collections.unmodifiableSet(features);
    }

    @Override
    public String toString() {
        return features.stream().map(feature -> trait.equals(Trait.NAME) ? feature.toString() // keeps camel case if it's a name
                : feature.toString().toLowerCase(Locale.ITALIAN)).collect(Collectors.joining(", ")) 
                + (features.isEmpty() ? "" : " ") + trait.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.trait, this.features);
    }

    @Override
    public boolean equals(final Object obj) {
        return obj instanceof Attribute ? trait.equals(((Attribute) obj).getTrait()) 
                && features.equals(((Attribute) obj).getFeatures()) : false;
    }

}
