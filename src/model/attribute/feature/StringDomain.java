package model.attribute.feature;

import utilities.Utilities;

/*
 * Domain implementation for String class, it is used as domain of the Trait name.
 */
class StringDomain extends AbstractDomain<String> {

    private static final StringDomain SINGLETON = new StringDomain();

    @Override
    public Class<String> getType() {
        return String.class;
    }

    @Override
    public String apply(final String string) {
        Utilities.requireNonNull(string);
        return string;
    }

    public static StringDomain getDomain() {
        return SINGLETON;
    }

}
