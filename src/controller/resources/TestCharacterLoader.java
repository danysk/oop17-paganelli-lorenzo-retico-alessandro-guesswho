package controller.resources;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;
import java.util.Set;
import org.junit.jupiter.api.Test;
import controller.gameoptions.GameSettings;
import controller.gameoptions.Pack;
import model.attribute.AttributeFactory;
import model.attribute.feature.PhysicalFeature;
import model.attribute.feature.Trait;
import model.character.Character;
import model.character.CharacterBuilder;

class TestCharacterLoader {

    private static final String WRONG = "Assertion was false";
    private static final int CHARACTERS = 24;
    private static final int NGLASSES = 5;
    private static final int WHITE_HAIRS = 5;
    private static final int NMALE = 19;

    @Test
    public void testClassicPack() {
        try {

            GameSettings.setPack(Pack.CLASSIC);
            final Set<Character> characters = CharacterLoader.loadCharacters();
            final CharacterBuilder builder = new CharacterBuilder();

            builder.add(AttributeFactory.from(Trait.HAIRS, PhysicalFeature.Color.WHITE, PhysicalFeature.Length.LONG, PhysicalFeature.HairStyle.STRAIGHT));
            builder.add(AttributeFactory.from(Trait.GENDER, PhysicalFeature.Gender.FEMALE));
            builder.add(AttributeFactory.from(Trait.EYES, PhysicalFeature.EyeColor.BROWN));
            builder.add(AttributeFactory.from(Trait.MOUTH, PhysicalFeature.Dimension.BIG));
            builder.add(AttributeFactory.from(Trait.NOSE, PhysicalFeature.Dimension.SMALL));
            builder.add(AttributeFactory.from(Trait.NAME, "Susana"));

            final Character susana = builder.build();
            assertTrue(susana.getAttributes().stream().filter(a -> a.getTrait().equals(Trait.GENDER) && a.getFeatures().contains(PhysicalFeature.Gender.FEMALE)).count() == 1, WRONG);
            assertTrue(characters.stream().map(c -> c.getAttributes()).
                    filter(a -> a.stream().anyMatch(b -> b.getTrait().equals(Trait.GENDER)
                            && b.getFeatures().contains(PhysicalFeature.Gender.MALE))).count() == NMALE, WRONG);
            assertTrue(characters.size() == CHARACTERS, WRONG);
            assertTrue(characters.stream().map(c -> c.getAttributes()).
                    filter(a -> a.stream().anyMatch(b -> b.getTrait().equals(Trait.HAIRS)
                            && b.getFeatures().contains(PhysicalFeature.Color.WHITE))).count() == WHITE_HAIRS, WRONG);
            assertTrue(characters.stream().filter(c -> c.getName().equals("Alejandro")).count() == 1, WRONG);
            assertTrue(characters.contains(susana), WRONG);
            assertTrue(characters.stream().map(c -> c.getAttributes()).
                    filter(a -> a.stream().anyMatch(b -> b.getTrait().equals(Trait.HAIRS)
                            && b.getFeatures().contains(PhysicalFeature.Color.WHITE))).count() == WHITE_HAIRS, WRONG);
            assertTrue(characters.stream().map(c -> c.getAttributes()).
                    filter(a -> a.stream().anyMatch(b -> b.getTrait().equals(Trait.MOUSTACHE)
                            && b.getFeatures().contains(PhysicalFeature.Color.BROWN))).count() == 2, WRONG);
            assertTrue(characters.stream().map(c -> c.getAttributes()).
                    filter(a -> a.stream().anyMatch(b -> b.getTrait().equals(Trait.GLASSES)))
                    .count() == NGLASSES, WRONG);
        } catch (LoadingException e) {
            fail("Loading exception occurred");
        }
    }
 
    @Test
    public void testSequentialLoading() {
        try {
            GameSettings.setPack(Pack.CLASSIC);
            final Set<Character> classic = CharacterLoader.loadCharacters();
            GameSettings.setPack(Pack.PULP_FICTION);
            final Set<Character> pulpfiction = CharacterLoader.loadCharacters();
            assertTrue(classic.stream().anyMatch(c -> !pulpfiction.contains(c)), WRONG);
            GameSettings.setPack(Pack.CLASSIC);
            assertTrue(classic.containsAll(CharacterLoader.loadCharacters()), WRONG);
        } catch (Exception e) {
            fail("Exception occurred");
        }
    }

    @Test
    public void testAddingCharacter() {
        try {
            GameSettings.setPack(Pack.PULP_FICTION);
            final Set<Character> characters = CharacterLoader.loadCharacters();
            final CharacterBuilder builder2 = new CharacterBuilder();

            builder2.add(AttributeFactory.from(Trait.GENDER, PhysicalFeature.Gender.MALE));
            builder2.add(AttributeFactory.from(Trait.NOSE, PhysicalFeature.Dimension.SMALL));
            builder2.add(AttributeFactory.from(Trait.GLASSES));
            builder2.add(AttributeFactory.from(Trait.NAME, "prova"));

            final Character prova = builder2.build();

            assertTrue(prova.getAttributes().stream().filter(a -> a.getTrait().equals(Trait.GENDER) && a.getFeatures().contains(PhysicalFeature.Gender.MALE)).count() == 1, WRONG);
            assertThrows(UnsupportedOperationException.class, () -> characters.add(prova), "Exception not thrown");
            assertFalse(characters.contains(prova), "Assertion was true");
        } catch (Exception e) {
            e.printStackTrace();
            fail("Exception occurred");
        }
    }
}
