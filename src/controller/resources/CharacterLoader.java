package controller.resources;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import controller.gameoptions.GameSettings;
import model.attribute.Attribute;
import model.attribute.AttributeFactory;
import model.attribute.feature.Trait;
import model.character.Character;
import model.character.CharacterBuilder;

/*
 * Loads the characters from the xml file, it gets the file name from GameSettings.getPack
 */
final class CharacterLoader {

    private static final String FILE_EXTENSION = ".xml";

    private CharacterLoader() {
    }

    public static Set<Character> loadCharacters() throws LoadingException {
        try (InputStream in = CharacterLoader.class.getResourceAsStream("/" + GameSettings.getPack() + FILE_EXTENSION)) {
            final Element rootElement = (Element) DocumentBuilderFactory.newInstance().newDocumentBuilder()
                        .parse(in)
                        .getElementsByTagName("characters")
                        .item(0);
            final NodeList charactersList = rootElement.getElementsByTagName("character");
            return Collections.unmodifiableSet(IntStream.range(0, charactersList.getLength()).mapToObj(n -> (Element) charactersList.item(n))
                 .map(characterElem -> new CharacterBuilder().addAll(CharacterLoader.readAttributes(characterElem)).build())
                 .collect(Collectors.toSet()));
        } catch (SAXException | IOException | ParserConfigurationException e) {
            throw new LoadingException("Error while loading characters");
        }
    }

    private static Set<Attribute> readAttributes(final Element characterElem) {
        return Arrays.stream(Trait.values())
                .filter(trait -> characterElem.getElementsByTagName(trait.toString()).getLength() > 0) //trait is present
                .map(trait -> AttributeFactory.from(trait, CharacterLoader.readFeatures(trait, (Element) 
                characterElem.getElementsByTagName(trait.toString()).item(0))))
                .collect(Collectors.toSet());
    }

    private static Set<?> readFeatures(final Trait trait, final Element traitElem) {
        return trait.getDomains().stream().filter(domain -> !traitElem.getAttribute(domain.getName()).isEmpty()) //keep feature only if present
                .map(domain -> domain.apply(traitElem.getAttribute(domain.getName())))
                .collect(Collectors.toSet());
    }

}
