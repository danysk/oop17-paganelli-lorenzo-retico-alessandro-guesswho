package controller.gameoptions;

import java.util.Optional;

import utilities.Utilities;

/**
 * A class collecting the game's settings.
 */
public final class GameSettings {

    private static Optional<Modality> modality = Optional.empty();
    private static Optional<Pack> pack = Optional.empty();
    private static Optional<Difficulty> difficulty = Optional.empty();

    private GameSettings() {
    }

    /**
     * Allows to set all the options.
     * @param modality 
     *                  the game's modality
     * @param pack 
     *                  the characters' pack
     * @param difficulty 
     *                  the game's difficulty
     */
    public static void setAll(final Modality modality, final Pack pack, final Difficulty difficulty) {
        setModality(modality);
        setDifficulty(difficulty);
        setPack(pack);
    }
    /**
     * Get game's modality.
     * @return the game's Modality
     */
    public static Modality getModality() {
        return modality.orElseThrow(UnsupportedOperationException::new);
    }
    /**
     * Set game's modality.
     * @param modality
     *                  the game's modality.
     */
    public static void setModality(final Modality modality) {
        Utilities.requireNonNull(modality);
        GameSettings.modality = Optional.of(modality);
    }
    /**
     * Get game's pack.
     * @return the game's Pack
     */
    public static Pack getPack() {
        return pack.orElseThrow(UnsupportedOperationException::new);
    }
    /**
     * Set game's modality.
     * @param pack
     *                  the game's pack.
     */
    public static void setPack(final Pack pack) {
        Utilities.requireNonNull(pack);
        GameSettings.pack = Optional.of(pack);
    }
    /**
     * Get game's difficulty.
     * @return the game's Difficulty
     */
    public static Difficulty getDifficulty() {
        return difficulty.orElseThrow(UnsupportedOperationException::new);
    }
    /**
     * Set game's difficulty.
     * @param difficulty
     *                          the game's difficulty.
     */
    public static void setDifficulty(final Difficulty difficulty) {
       Utilities.requireNonNull(difficulty);
       GameSettings.difficulty = Optional.of(difficulty);
   }
}
