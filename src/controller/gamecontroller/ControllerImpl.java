package controller.gamecontroller;

import java.util.*;
import java.util.logging.*;
import java.util.stream.IntStream;
import controller.gameoptions.Difficulty;
import controller.gameoptions.GameSettings;
import controller.gameoptions.Modality;
import controller.gameoptions.Pack;
import controller.playercontroller.CpuController;
import controller.playercontroller.HumanController;
import controller.playercontroller.PlayerController;
import controller.playercontroller.PlayerController.Status;
import controller.resources.LoadingException;
import controller.resources.Resources;
import model.question.Question;
import utilities.Messages;
import utilities.Utilities;
import view.LogView;
import view.StartingView;
import view.viewposition.PositionManager;

/**
 * Controller implementation.
 */
public class ControllerImpl implements Controller {

    private enum GameStatus {
        STARTING, ONGOING, ENDED;
    }

    private static final int NUMBER_OF_PLAYERS = 2;
    private static final Logger LOG = Logger.getAnonymousLogger();

    private final List<PlayerController> players = new ArrayList<>();
    private PlayerController current;
    private int playersInGame;
    private GameStatus status = GameStatus.STARTING;

    /**
     * Constructor.
     * @throws LoadingException 
     *              in case of error during loading of resources.
     * @param modality 
     *              the game's modality.
     * @param difficulty 
     *              the game's difficulty.
     * @param pack
     *              the game's pack.
     */
    public ControllerImpl(final Modality modality, final Difficulty difficulty, final Pack pack) throws LoadingException {
        Utilities.requireNonNull(modality, difficulty, pack);
        GameSettings.setAll(modality, pack, difficulty);
        Resources.load();
        PositionManager.createPositions();
        LOG.addHandler(new FileHandler());
        if (modality == Modality.SIMULATION) {
           LOG.addHandler(new ViewHandler(new LogView()));
        }
        IntStream.range(0, NUMBER_OF_PLAYERS).forEach(n -> players.add(n, n < GameSettings.getModality().getNumberOfHumans() 
                ? new HumanController(this) : new CpuController(this)));
        current = Utilities.getRandom(players);
        this.checkReady();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void selected(final PlayerController player, final String characterName) {
        this.checkStatus(GameStatus.STARTING);
        Utilities.requireNonNull(player, characterName);
        playersInGame++;
        LOG.log(Level.INFO, Messages.HAS_SELECTED + characterName, (player instanceof HumanController ? "[Human]" : "[Cpu]") + "Player " 
                + (players.contains(player) ? players.indexOf(player) + 1 : players.size() + 1)); 
        this.checkReady();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void askOpponent(final Question question) {
        this.checkStatus(GameStatus.ONGOING);
        Utilities.requireNonNull(question);
        try {
            this.logg(current, question.toString());
            final boolean answer = getOpponent().askPlayer(question);
            this.logg(getOpponent(), answer ? Messages.YES : Messages.NO);
            current.registerAnswer(question, answer);
            this.endTurn(current.getStatus());
        } catch (InterruptedException e) {
            //notifies the interruption of an action, nothing else should be done
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public void quit(final PlayerController player) {
        Utilities.requireNonNull(player);
        if (status != GameStatus.ENDED) { //player quit before the ending of the game
            status = GameStatus.ENDED;
            playersInGame = NUMBER_OF_PLAYERS - 1;
            this.logg(player.equals(current) ? current : getOpponent(), Messages.EXIT);
            (player.equals(current) ? getOpponent() : current).endGame(Status.OPPONENT_QUIT, Optional.empty());
        } else {
            playersInGame--;
            if (playersInGame == 0) {
                Arrays.stream(LOG.getHandlers()).forEach(Handler::close);
                if (GameSettings.getModality() != Modality.SIMULATION) {
                    new StartingView(); //otherwise the starting view is opened by the log view when closed
                }
             }
        }
    }

    private void endTurn(final Status status) {
        if (status != Status.PLAYING) {
            this.status = GameStatus.ENDED;
            final Status opponentStatus = status == Status.WON ? Status.LOST : Status.WON;
            this.logg(current, "Has " + status.name().toLowerCase(Locale.ITALIAN));
            this.logg(getOpponent(), "Has " + opponentStatus.name().toLowerCase(Locale.ITALIAN));
            current.endGame(status, Optional.of(getOpponent().getSelected()));
            getOpponent().endGame(opponentStatus, Optional.of(current.getSelected()));
        } else {
            current = getOpponent();
            current.play();
        }
    }

    private void logg(final PlayerController player, final String msg) {
        LOG.log(Level.INFO, msg, this.getName(player));
    }

    private String getName(final PlayerController player) {
        return (player instanceof HumanController ? "[Human]" : "[Cpu]") + "Player " + (players.indexOf(player) + 1);
    }

    private void checkReady() {
        if (playersInGame == NUMBER_OF_PLAYERS && players.size() == NUMBER_OF_PLAYERS) {
            status = GameStatus.ONGOING;
            current.play();
        }
    } 

    private PlayerController getOpponent() {
        return players.get((players.indexOf(current) + 1) % NUMBER_OF_PLAYERS);
    }

    private void checkStatus(final GameStatus required) {
        if (!status.equals(required)) {
            throw new IllegalStateException("Game status is: " + status);
        }
    }

}
