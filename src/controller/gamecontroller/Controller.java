package controller.gamecontroller;

import controller.playercontroller.PlayerController;
import model.question.Question;

/**
 * Modeling interface for the main Controller of the game: manages the creation of View/Player(s) 
 * and the progress of the game. This interface also defines the way a Player can interact with his opponent.
 */
public interface Controller {

    /**
     * Allows Players to communicate the selected Character.
     * @param player 
     *                  the Controller of the Player
     * @param characterName 
     *                  the name of the selected Character
     */
    void selected(PlayerController player, String characterName);

    /**
     * Allows Players to ask a Question to the opponent.
     * @param question 
     *                  the Question to ask.
     */
    void askOpponent(Question question);

    /**
     * Allows Players to properly quit the game.
     * @param player 
     *                  the Controller of the Player who wants to quit.
     */
    void quit(PlayerController player);

}
