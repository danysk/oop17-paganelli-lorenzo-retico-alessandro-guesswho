package view;

import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.*;
import utilities.Messages;
import utilities.Utilities;
import view.viewposition.PositionManager;
import view.viewposition.ViewPosition;

/**
 * View used to show the game's log in case of Simulation modality.
 */
public class LogView extends JFrame {

    private static final long serialVersionUID = 1L;
    private static final int WIDTH_PROPORTION = 3;
    private static final int HEIGHT_PROPORTION = 2;

    private final JPanel panel = new JPanel();

    /**
     * Constructor.
     */
    public LogView() {
        super();
        final JScrollPane scrollPanel = new JScrollPane(panel);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        this.add(scrollPanel);
        this.setMinimumSize(new Dimension(Utilities.getScreenDimension().width / WIDTH_PROPORTION, 
                Utilities.getScreenDimension().height / HEIGHT_PROPORTION));
        this.pack();
        this.setLocation(PositionManager.getSpecificPosition(this.getSize(), ViewPosition.CENTER));
        this.setTitle(Messages.TITLE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent e) {
                dispose();
                new StartingView();
            }
        });
        this.setVisible(true);
    }

    /**
     * Writes a message on the View.
     * @param message 
     *                  the message to write
     */
    public void write(final String message) {
        Utilities.requireNonNull(message);
        panel.add(new JLabel(message));
    }

}
